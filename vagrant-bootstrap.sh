#! /bin/bash

# Update repositories to correctly locate required packages.

apt-get update

# Basic tools setup, such as pip, git, etc.

apt-get -y install python-pip python-dev build-essential git

# PostgreSQL setup, required to install heroku's 'django-toolbelt'.

apt-get -y install libpq-dev postgresql postgresql-contrib

# Project requirements setup (pip).

cd /vagrant && pip install -r requirements.txt
