# coding: utf-8

import dj_database_url

from deploy_status.settings import *

DEBUG = False

ALLOWED_HOSTS = ["*"]

DATABASES["default"] = dj_database_url.config()

STATIC_ROOT = "staticfiles"
