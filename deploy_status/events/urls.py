# coding: utf-8

from django.conf.urls import patterns, url

from deploy_status.events.views import EventStreamView

urlpatterns = patterns("",
    url(r"^stream/$", EventStreamView.as_view(), name="stream"),
)
