# coding: utf-8

import time

from django.conf import settings
from django_sse.views import BaseSseView

class EventStreamView(BaseSseView):

    def dispatch(self, request, *args, **kwargs):
        # This view is supposed to be acessed by a chrome
        # extension, which utility is to be the client of
        # this application, so we must allow CORS to its origin.
        # TODO: Remove the CORS headers from view logic.
        response = super(EventStreamView, self).dispatch(request, *args, **kwargs)
        response["Access-Control-Allow-Origin"] = settings.CHROME_EXTENSION_ORIGIN
        return response

    def iterator(self):
        while True:
            self.sse.add_message("deploy_action", "data")
            yield
            time.sleep(5)
