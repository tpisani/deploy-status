# coding: utf-8

from django.contrib import admin
from django.conf.urls import patterns, include, url

admin.autodiscover()

urlpatterns = patterns("",
    url(r"^events/", include("deploy_status.events.urls", namespace="events")),
    url(r"^admin/", include(admin.site.urls)),
)
